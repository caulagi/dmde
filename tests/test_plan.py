import pytest

from dmde import plan


@pytest.mark.asyncio
async def test_plan_paris():
    assert await plan.plan(
        {
            "DM_capacity": 20,
            "DE_capacity": 8,
            "data_centers": [
                {"name": "Paris", "servers": 20},
                {"name": "Stockholm", "servers": 62},
            ],
        }
    ) == {"DE": 8, "DM_data_center": "Paris"}


@pytest.mark.asyncio
async def test_plan_stockholm():
    assert await plan.plan(
        {
            "DM_capacity": 6,
            "DE_capacity": 10,
            "data_centers": [
                {"name": "Paris", "servers": 30},
                {"name": "Stockholm", "servers": 66},
            ],
        }
    ) == {"DE": 9, "DM_data_center": "Stockholm"}


@pytest.mark.asyncio
async def test_plan_berlin():
    assert await plan.plan(
        {
            "DM_capacity": 12,
            "DE_capacity": 7,
            "data_centers": [
                {"name": "Berlin", "servers": 11},
                {"name": "Stockholm", "servers": 21},
            ],
        }
    ) == {"DE": 3, "DM_data_center": "Berlin"}


@pytest.mark.asyncio
async def test_plan_five_dc():
    assert await plan.plan(
        {
            "DM_capacity": 12,
            "DE_capacity": 7,
            "data_centers": [
                {"name": "Paris", "servers": 11},
                {"name": "Stockholm", "servers": 21},
                {"name": "Berlin", "servers": 31},
                {"name": "London", "servers": 14},
                {"name": "Singapore", "servers": 3},
            ],
        }
    ) == {"DE": 11, "DM_data_center": "Paris"}
