from starlette.testclient import TestClient

from dmde.app import app


def test_400_server():
    client = TestClient(app)
    response = client.post(
        "/",
        json={"DM_capacity": 20, "DE_capacity": 8, "data_centers": [{"name": "Paris"}]},
    )
    assert response.status_code == 400


def test_400_dm_capacity():
    client = TestClient(app)
    response = client.post(
        "/",
        json={"DM_capacity": 20, "data_centers": [{"name": "Paris", "servers": 20}]},
    )
    assert response.status_code == 400


def test_success():
    client = TestClient(app)
    response = client.post(
        "/",
        json={
            "DM_capacity": 20,
            "DE_capacity": 8,
            "data_centers": [
                {"name": "Paris", "servers": 20},
                {"name": "Stockholm", "servers": 62},
            ],
        },
    )
    assert response.status_code == 200
    assert response.json() == {"DE": 8, "DM_data_center": "Paris"}
