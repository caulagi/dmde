import itertools
import sys
from math import ceil


def _flatten(listOfLists):
    """Flatten one level of nesting

    From https: // docs.python.org/3.7/library/itertools.html
    """
    return itertools.chain.from_iterable(listOfLists)


def _get_options(dm, de, server):
    """Get all possible combinations of dm,de that will satify server requirements

    >>> get_options(20, 8, 20)
    ((20,), (8, 8, 8))
    >>> get_options(20, 8, 62)
    ((20, 8, 8, 8, 8, 8, 8), (8, 8, 8, 8, 8, 8, 8, 8, 8))
    """
    # in python 3, this DOESN'T use integer division
    without_dm = [de for i in range(ceil(server / de))]
    if server <= dm:
        with_dm = (dm,)
    else:
        # get a list of de's where len of list = number of de required to match remaining server capacity
        de_list = [de for i in range(ceil((server - dm) / de))]
        with_dm = list(itertools.chain((dm,), de_list))
    return (with_dm, without_dm)


def _is_valid(possible_solution, dm):
    "Return True if there is exactly one dm in possible_solution, else False"
    return len(list(filter(lambda x: dm in x, possible_solution))) == 1


def _get_num_de(option):
    # 1 represents the DM
    return len(list(_flatten(option))) - 1


def _finalize(valid_options, server_names, dm_capacity):
    "Return the min number of de and dm server name for these valid options"
    min_de = sys.maxsize
    for option in valid_options:
        length = _get_num_de(option)
        if length < min_de:
            min_de = length
            min_de_option = option
    num_de = _get_num_de(min_de_option)
    for i in range(len(min_de_option)):
        if dm_capacity in min_de_option[i]:
            dm_server = server_names[i]
            break
    return (num_de, dm_server)


async def plan(params):
    # list used to get possible combinations of dm and de's
    options = []
    # used to identify the datacenter for dm
    server_names = []
    # list of valid solutions
    valid_options = []

    for row in params["data_centers"]:
        server_names.append(row["name"])
        options.append(
            _get_options(params["DM_capacity"], params["DE_capacity"], row["servers"])
        )

    # itertools.product is the heart of the solution
    # for example of -
    # "DM_capacity": 20, "DE_capacity": 8, "data_centers": [
    #   {"name": "Paris", "servers": 20}, { "name": "Stockholm", "servers": 62 }
    # ]
    # options = [((20,), [8, 8, 8]), ([20, 8, 8, 8, 8, 8, 8], [8, 8, 8, 8, 8, 8, 8, 8])]
    #
    # and itertools.product(*options) =
    # ((20,), [20, 8, 8, 8, 8, 8, 8])
    # ((20,), [8, 8, 8, 8, 8, 8, 8, 8])
    # ([8, 8, 8], [20, 8, 8, 8, 8, 8, 8])
    # ([8, 8, 8], [8, 8, 8, 8, 8, 8, 8, 8])
    #
    for possible_solution in itertools.product(*options):
        if _is_valid(possible_solution, params["DM_capacity"]):
            valid_options.append(possible_solution)

    num_de, dm_server = _finalize(valid_options, server_names, params["DM_capacity"])
    return {"DE": num_de, "DM_data_center": dm_server}
