import json

import uvicorn
from starlette.applications import Starlette
from starlette.exceptions import HTTPException
from starlette.responses import JSONResponse

from dmde import plan

app = Starlette(debug=True)


@app.route("/", methods=["POST"])
async def homepage(request):
    try:
        body = await request.json()
    except json.decoder.JSONDecodeError:
        raise HTTPException(status_code=400)

    for required in ("DM_capacity", "DE_capacity", "data_centers"):
        if required not in body:
            raise HTTPException(status_code=400)
    for server in body["data_centers"]:
        for required in ("name", "servers"):
            if required not in server:
                raise HTTPException(status_code=400)
    return JSONResponse(await plan.plan(body))


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
