# dmde

A simple python api to check some scheduling logic.

The project uses [starlette][starlette] and requires python 3.6+.

## Run using docker

```shell
$ docker build -t dmde .
$ docker run -p 8000:8000 dmde python -m dmde.app

# in a different shell
$ curl -X POST -H "content-type: application/json" -d @test.json http://localhost:8000
```

## Run locally

```shell
$ python3 -m venv .venv
$ source ./.venv/bin/activate
$ pip install -r requirements-dev.txt

$ python -m dmde.app

# in a different shell
$ curl -X POST -H "content-type: application/json" -d @test.json http://localhost:8000
```

## Tests

```shell
# using docker
$ docker run dmde pytest -v 

# locally
$ pytest -v
```


[starlette]: https://www.starlette.io/
