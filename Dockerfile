FROM python:3.7-alpine3.9

RUN apk --update add build-base
COPY . /code
WORKDIR /code

RUN pip install -r requirements-dev.txt


